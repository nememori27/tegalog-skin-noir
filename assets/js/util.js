const scroll_to_top_btn = document.getElementById('scrolltop');

scroll_to_top_btn.addEventListener('click', () => {
  window.scrollTo({ top: 0, behavior: 'smooth' });
});

window.addEventListener('scroll', () => {
  scroll_to_top_btn.style.display = window.scrollY > 400 ? 'block' : 'none';
});
