Copyright (c) 2022 htrkwn
Released under the MIT license
https://opensource.org/licenses/MIT

# Tegalog Skin - Noir

対応てがろぐバージョン: 3.8.4β or higher

## Information

[にししふぁくとりー](https://www.nishishi.com/)様にて配布されている、
[お手軽マイクロブログCGI「てがろぐ」](https://www.nishishi.com/cgi/tegalog/)
用のスキン

## Special Thanks

[Bulma](https://bulma.io/) (under the MIT License)  
Copyright (c) 2022 Jeremy Thomas.  
https://github.com/jgthms/bulma/blob/master/LICENSE

[Feather](https://feathericons.com/) (under the MIT License)  
Copyright (c) 2013-2017 Cole Bemis  
https://github.com/feathericons/feather/blob/master/LICENSE

[GLightbox](https://biati-digital.github.io/glightbox/) (under the MIT License)  
Copyright (c) 2018 Biati Digital https://www.biati.digital  
https://github.com/biati-digital/glightbox/blob/master/LICENSE.md

## 免責事項

このスキンを使用した上で被ったいかなる損害について、一切責任を負うものではございませんのであらかじめご了承ください。
